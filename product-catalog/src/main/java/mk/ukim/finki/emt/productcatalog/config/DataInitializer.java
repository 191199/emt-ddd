package mk.ukim.finki.emt.productcatalog.config;

import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.productcatalog.domain.models.Product;
import mk.ukim.finki.emt.productcatalog.repository.ProductRepository;
import mk.ukim.finki.emt.sharedkernel.base.financial.Currency;
import mk.ukim.finki.emt.sharedkernel.base.financial.Money;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DataInitializer {
  private final ProductRepository productRepository;

  @PostConstruct
  public void initData() {
    Product p1 = Product.build("Pizza", Money.valueOf(Currency.MKD,500), 10);
    Product p2 = Product.build("Ice Cream", Money.valueOf(Currency.MKD,100), 5);
    if (productRepository.findAll().isEmpty()) {
      productRepository.saveAll(Arrays.asList(p1,p2));
    }
  }

}
