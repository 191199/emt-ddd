package mk.ukim.finki.emt.productcatalog.domain.models;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.base.domain.AbstractEntity;
import mk.ukim.finki.emt.sharedkernel.base.financial.Money;
import mk.ukim.finki.emt.productcatalog.domain.valueobjects.Quantity;

@Entity
@Table(name = "product")
@Getter
public class Product extends AbstractEntity<ProductId> {
  private String productName;

  private int sales = 0;

  @AttributeOverrides({
      @AttributeOverride(name="amount", column = @Column(name = "price_amount")),
      @AttributeOverride(name="currency", column = @Column(name = "price_currency"))
  })
  private Money price;

  private Product() {
    super(ProductId.randomId(ProductId.class));
  }

  public static Product build(String productName, Money price, int sales) {
    Product p = new Product();
    p.price = price;
    p.productName = productName;
    p.sales = sales;
    return p;
  }

  public void addSales(int qty) {
    this.sales = this.sales - qty;
  }

  public void removeSales(int qty) {
    this.sales -= qty;
  }

}
