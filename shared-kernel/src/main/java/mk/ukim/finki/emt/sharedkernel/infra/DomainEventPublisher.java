package mk.ukim.finki.emt.sharedkernel.infra;

import mk.ukim.finki.emt.sharedkernel.base.domain.events.DomainEvent;

public interface DomainEventPublisher {
  void publish(DomainEvent event);
}
