package mk.ukim.finki.emt.sharedkernel.base.financial;

public enum Currency {
  EUR, USD, MKD
}
