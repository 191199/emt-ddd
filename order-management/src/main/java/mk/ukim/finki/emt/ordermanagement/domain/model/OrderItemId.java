package mk.ukim.finki.emt.ordermanagement.domain.model;

import mk.ukim.finki.emt.sharedkernel.base.domain.DomainObjectId;

public class OrderItemId extends DomainObjectId {
  private OrderItemId() {
    super(OrderItemId.randomId(OrderItemId.class).getId());
  }

  public OrderItemId(String uuid) {
    super(uuid);
  }

}
