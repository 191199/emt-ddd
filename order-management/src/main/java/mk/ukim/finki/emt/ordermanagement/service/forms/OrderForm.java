package mk.ukim.finki.emt.ordermanagement.service.forms;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import mk.ukim.finki.emt.sharedkernel.base.financial.Currency;

@Data
public class OrderForm {
  @NotNull
  private Currency currency;

  @Valid
  @NotEmpty
  private List<OrderItemForm> items = new ArrayList<>();
}
