package mk.ukim.finki.emt.ordermanagement.domain.valueobjects;

import javax.persistence.Embeddable;
import mk.ukim.finki.emt.sharedkernel.base.domain.DomainObjectId;

@Embeddable
public class ProductId extends DomainObjectId {
  private ProductId() {
    super(ProductId.randomId(ProductId.class).getId());
  }

  public ProductId(String uuid) {
    super(uuid);
  }

}
